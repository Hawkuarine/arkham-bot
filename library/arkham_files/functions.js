// Adds one to the room number, then outputs the current room number to the channel.
exports.GenerateRoom = (m,room) => {
    room++
    m.respond(`You\'re now in room ${room}.`)
    return room;
}

// Choose the event that will occur in the room
exports.GenerateEvent = () => {

}

// Generate an enemy encounter
exports.GenerateEnemy = () => {

}

// Quit the game
exports.QuitGame = (m,gamestarted) => {

    // Output to channel
    if (gamestarted == true) { m.respond('The game in progress has been quit.') }
    else {m.respond('There is no game currently in progress.')}

    // Reset game
    return gamestarted = false;
}

// Set room number
exports.SetRoom = (set,m) => {

    // Set new room number
    room = set;

    // Ouput to channel
    m.respond(`Current room set to **${room}**.`);
    return;
}