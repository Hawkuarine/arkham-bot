const { bot, functions } = require('../../bot')
const { quitgame } = require('../../library/arkham_files/functions');

// Run this
exports.run = (m, a) => 
{ 
  quitgame(m);
}

// Meta
exports.meta = 
{
    names: ['quit'], 
    permissions: [], 
    help: {
        description: 'Quits the game currently in progress.', // The description of the command used in help
        usage: '', // Usage of the command. Use [] for optional arugments: !ping [@member]
        category: 'game' // The category used in help
    }
}