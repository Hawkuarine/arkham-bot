const { bot, functions } = require('../../bot')
const {GenerateRoom, GenerateEnemy, GenerateEvent, QuitGame} = require('../../library/arkham_files/functions')
let gamestarted, room

// Run this
exports.run = (m, a) => { 
    if (a == 'quit')
    {
        QuitGame(m,gamestarted);
        gamestarted = false;
        return;
    }

    if (a == 'play' || 'p')
    {
        if (!gamestarted)
            {
                gamestarted = true
                room = 0;
                GenerateRoom(m,room)
            }
        else
        {
            m.respond('There is a game already in progress.');
        }
    }
}

// Meta
exports.meta = {
    names: ['game','g'], 
    permissions: [], 
    help: {
        description: 'Play Arkham.', 
        usage: '[play/quit]', 
        category: 'game'}
}