const { modules } = require('../../bot');
const { setroom } = require('../game/game');

exports.run = (m,a) => {

    // Check argument
    if (a.length == 0)
    {
        m.respond('Not enough arguments.','An error occurred.')
        return;
    }
    else
    {
        setroom(a,m);
    }

}

exports.meta = {
    names: ['sr', 'setroom'],
    permissions: ['BOT_OWNER'],
    help: {
        description: 'Set current game\'s room number.',
        usage: '[room]',
        category: 'dev'
    }
}