const { bot, modules, functions } = require('../../bot')
const { help } = modules.get('commandLoader')

exports.run = (m, a) => {
    // Get the prefix
    const prefix = functions.getPrefix(m.guild.id)

    // Create an embed and set the title
    const embed = functions.embed()
        .setAuthor('Commands', bot.user.displayAvatarURL)

    const sendList = () => {
        // Generate list of categories
        const list = Array.from(help.keys())
            .map(category => `\`${prefix}help ${category}\``)

        // Add the list to the embed
        embed.addField('Help Categories:', `\`${prefix}help all\`\n${list.join('\n')}`)
        // Send the embed
        m.channel.send({embed})
    }

    // Send the list of categories if no arguments are provided
    if (a.length < 1 || !a[0] || typeof a[0] != 'string') return sendList()
    
    // If a valid category is provided
    if (Array.from(help.keys()).includes(a[0].toLowerCase())) {
        // Generate the help for this category
        const list = help.get(a[0].toLowerCase())
            .map(item => `\`${prefix}${item.names[0]}\` ${item.description}${item.usage != '' ? ` \`${prefix}` +
                `${item.names[0]} ${item.usage}\`` : ''}`)
        
        // Add the list to the embed
        embed.addField(a[0][0].toUpperCase() + a[0].slice(1).toLowerCase(), list.join('\n'))
        // Send the embed
        m.channel.send({embed})
        return
    
    // If the user wants all help
    } else if (a[0].toLowerCase() == 'all') {
        // Generate help for all categories
        Array.from(help.keys()).forEach(key => {
            const list = help.get(key)
                .map(item => `\`${prefix}${item.names[0]}\` ${item.description}${item.usage != '' ? ` \`${prefix}` +
                    `${item.names[0]} ${item.usage}\`` : ''}`)
            
            // Add them to the embed
            embed.addField(key[0].toUpperCase() + key.slice(1).toLowerCase(), list.join('\n'))
        })

        // Send the embed
        m.channel.send({embed})
        return
    
    // Otherwise, send the list of categories
    } else sendList()
}

exports.meta = {
    names: ['help'],
    permissions: [],
    help: {
        description: 'See the list of all commands',
        usage: '',
        category: 'examples'
    }
}