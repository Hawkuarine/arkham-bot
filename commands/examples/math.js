const { } = require('../../bot')

const commandenabled = false;

exports.run = (m, a) => {
    if (commandenabled == true)
    {
        const filter = msg => msg.content.startsWith('> ') && msg.author.id == m.author.id
        const collector = m.channel.createMessageCollector(filter, { time: 60 * 1000, maxMatches: 1 })
    
        m.respond('You could just use a calculator, but OK.','**[>(number)+(number)...]**')
    
        collector.on('collect', msg => 
        {
            const math_answer = msg.content
                .replace(/>|\s*/g, '')
                .split('+')
                .map(x => parseInt(x,10))
                .reduce((a, b) => a + b)
            m.respond(`The answer is **${math_answer}**.`)
        })
    
        collector.on('end', reason => 
        {
            if (reason == 'time') m.respond('Timeout')
        })  
    }
    if (commandenabled == false)
    {
        m.respond('Command is disabled lol')
    }
}

exports.meta = 
{
    names: ['math', 'add'],
    permissions: [],
    help: 
    {
        description: 'Does addition',
        usage: '',
        category: 'examples'
    }
}