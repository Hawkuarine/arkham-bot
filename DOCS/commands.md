# Help
## Dev
### Asynceval
- Run code asynchronously 
- Aliases: `ae`
- Usage: `asynceval await someCode()`
- You need `BOT OWNER` to run this command
### Blacklist
- Blacklist a user
- You need `BOT OWNER` to run this command
### Eval
- Run code
- Aliases: `e`
- Usage: `eval someCode()`
- You need `BOT OWNER` to run this command
### Genhelpmd
- Generates help documentation in markdown
- You need `BOT OWNER` to run this command
### Reload
- Reload the commands without restarting the bot
- Aliases: `rel`, `r`
- Usage: `reload [silent]`
- You need `BOT OWNER` to run this command
### Silenteval
- Run code silently
- Aliases: `se`
- Usage: `silenteval someCode()`
- You need `BOT OWNER` to run this command
### Stop
- Stop the bot
- Aliases: `kys`
- You need `BOT OWNER` to run this command
## Examples
### Help
- See the list of all commands
### Ping
- See the bot's ping
- Aliases: `pong`
### Prefix
- Change the prefix in this server
- Aliases: `setprefix`
- Usage: `prefix [new prefix]`
- You need `MANAGE MESSAGES` to run this command